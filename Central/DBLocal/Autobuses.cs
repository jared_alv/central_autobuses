﻿namespace Central.DBLocal
{
	public class Autobuses:Base
	{
		public int numAsientos { get; set; }
		public string Marca { get; set; }
		public string Modelo { get; set; }
		public DateTime fechaAdquisicion { get; set; }
	}
}
