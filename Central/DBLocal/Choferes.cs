﻿namespace Central.DBLocal
{
	public class Choferes:Base
	{
		public string Nombre { get; set; }
		public string ApellidoPa { get; set; }
		public string ApellidoMa { get; set; }
		public string Domicilio { get; set; }
		public string Ciudad { get; set; }
		public string Estado { get; set; }
		public string Rfc { get; set; }
		public string CedulaChofer { get; set; }

	}
}
