﻿namespace Central.DBLocal
{
	public class Corridas:Base
	{
		public string LugarDestino { get; set; }
		public string LugarOrigen { get; set; }
		public DateTime FechaSalida { get; set; }
		public DateTime HoraSalida { get; set; }
		public string IdChofer { get; set; }
		public string IdAutobus { get; set; }
		public float PrecioAdulto { get; set; }
		public float PrecioNiño { get; set; }
		public string TipoServicio { get; set; }
        public string IdSucursal { get; set; }

	}
}
