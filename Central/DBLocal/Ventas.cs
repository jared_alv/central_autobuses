﻿namespace Central.DBLocal
{
	public class Ventas:Base
	{
		public string NomUsuario { get; set; }
		public string IdCaja { get; set; }
		public float ImporteVenta { get; set; }
		public DateTime FechaVenta { get; set; }
		public float numBoletos { get; set; }
	}
}
