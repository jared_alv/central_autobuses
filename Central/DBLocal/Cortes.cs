﻿namespace Central.DBLocal
{
	public class Cortes : Base
	{
		public string NomUsuario { get; set; }
		public int NumVentas { get; set; }
		public string NumCancelaciones { get; set; }
		public float ImporteVentas { get; set; }
		public float ImporteCancelaciones { get; set; }
		public float EfectivoTotal { get; set; } 
	    public DateTime FechaCorte { get; set; }

	

	}
}
