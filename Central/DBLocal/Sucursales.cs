﻿namespace Central.DBLocal
{
	public class Sucursales:Base
	{
		public string NomSucursal { get; set; }
		public string CalleSucursal { get; set; }
		public string ColoniaSucursal { get; set; }
		public string CiudadSucursal { get; set; }
		public string EstadoSucursal { get; set; }

	}
}
