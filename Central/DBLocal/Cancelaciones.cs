﻿namespace Central.DBLocal
{
	public class Cancelaciones:Base
	{
		public string IdBoleto { get; set; }
		public float Importe { get; set; }
		public DateTime FechaCancelacion { get; set; }
		public string IdCaja { get; set; }
		public string NumUsuario { get; set; }
	}
}
