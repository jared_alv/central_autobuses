﻿using System.Data.SqlTypes;

namespace Central.DBLocal
{
	public class Boletos:Base
	{
		public string IdCorrida { get; set; }
		public string LugarDestino { get; set; }
		public string LugarSalida { get; set; }
		public string NombreCliente { get; set; }
		public int Edad { get; set; }
		public string IdVenta { get; set; }
		public DateTime FechaSalida { get; set; }
		public float Precio { get; set; }
		public string Clase { get; set; }
		public string numAsiento { get; set; }
		public string IdAutobuses { get; set; }
		public string IdSucursal { get; set; }
	}
}
