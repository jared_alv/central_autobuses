﻿using Central.DBLocal;
using FluentValidation;

namespace Central.Validadores
{
    public class GenericValidator<T> : AbstractValidator<T> where T : Base
    {
        public GenericValidator()
        {
			//Prueba de Cambio
			RuleFor(x => x.Id).NotEmpty();

		}
    }
}
