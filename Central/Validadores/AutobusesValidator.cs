﻿using Central.DBLocal;
using FluentValidation;

namespace Central.Validadores
{
	public class AutobusesValidator:GenericValidator<Autobuses>
	{
		public AutobusesValidator()
		{
			RuleFor(x => x.numAsientos).NotNull().NotEmpty();
			RuleFor(x => x.Marca).NotEmpty().NotNull().MaximumLength(50);
			RuleFor(x => x.Modelo).NotEmpty().NotNull().MaximumLength(50);
			RuleFor(x => x.fechaAdquisicion).NotEmpty().NotNull();


		}
	}
}
